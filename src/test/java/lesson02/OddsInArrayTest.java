package lesson02;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import lesson02.oddinarray.Solution;

public class OddsInArrayTest {
    private Solution s = new Solution();

    @Test
    public void shouldFindOddInExampleArray() {
        assertEquals(7, s.solution(new int[] {9, 3, 9, 3, 9, 7, 9}));
    }

    @Test
    public void shouldHandleArrayWithOneElement() {
        assertEquals(2, s.solution(new int[] {2}));
    }

    @Test
    public void shouldFindOddInLastPosition() {
        assertEquals(4, s.solution(new int[] {2, 2, 3, 3, 4}));
    }
}
