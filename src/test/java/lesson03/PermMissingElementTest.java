package lesson03;

import lesson03.permmissingelement.Solution;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PermMissingElementTest {

    private final Solution s = new Solution();

    @Test
    public void shouldWorkWithExample(){
        assertEquals(4, s.solution(new int[]{2, 3, 1, 5}));
    }

}
