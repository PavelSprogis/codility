package lesson03;

import lesson03.frogjmp.Solution;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class FrogJmpTest {
    private final Solution s = new Solution();

    @Test
    public void shouldWorkWithExampleTest() {
        assertEquals(3, s.solution(10, 85, 30));
    }

    @Test
    public void shouldWorkInBoundaryConditions() {
        assertEquals(9, s.solution(10, 100, 10));
    }

    @Test
    public void shouldReturnZeroIfXequalsY() {
        assertEquals(0, s.solution(90, 90, 30));
    }

    @Test(timeout = 100)
    public void shouldCalculateBigJump() {
        assertEquals(142730189, s.solution(3, 999111321, 7));
    }
}
